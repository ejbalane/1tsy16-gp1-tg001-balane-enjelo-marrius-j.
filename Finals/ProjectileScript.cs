﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {

	// Use this for initialization
    GameObject prefab;
	void Start () {
        prefab = Resources.Load("Projectile") as GameObject;
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetMouseButtonDown(0))
        {
            GameObject Projectile = Instantiate(prefab) as GameObject;
            Projectile.transform.position = transform.position + Camera.main.transform.forward * 2;
            Rigidbody rb = Projectile.GetComponent<Rigidbody>();
            rb.velocity = Camera.main.transform.forward * 40;


        }
	}
}
