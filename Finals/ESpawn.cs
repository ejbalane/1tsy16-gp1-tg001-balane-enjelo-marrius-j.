﻿using UnityEngine;
using System.Collections;

public class ESpawn : MonoBehaviour {
    private Camera camera;
    private GameObject Enemy;
    public GameObject EShip;
    public GameObject EShip1;
    public GameObject Shark;
	// Use this for initialization
	void Start () {
        GameObject cameraObject = GameObject.FindGameObjectWithTag("MainCamera");
        camera = cameraObject.GetComponent<Camera>();
        StartCoroutine(EnemySpawn());
	}
    IEnumerator EnemySpawn() {
        for (int i = 0; i < 1; i = 0) {
            int a = Random.Range(1, 3);
            int b = Random.Range(1, 3);
            int SpawnEnemy = Random.Range(1, 3);
            if (SpawnEnemy == 1) 
                Enemy = Shark;            
            else if (SpawnEnemy == 2)
                Enemy = EShip;            
            else            
                Enemy = EShip1;
            Enemy.transform.position = new Vector3(a, 2, b);
            Vector3 position = camera.ViewportToWorldPoint(Enemy.transform.position);
            Instantiate(Enemy, position, Quaternion.identity);
            yield return new WaitForSeconds(10);
        }
    }
}
