﻿using UnityEngine;
using System.Collections;

public class MouseMove : MonoBehaviour {
    public float mouseSpeed;
    public GameObject Mouse;
	// Use this for initialization
	void Start () {
        GetComponent<MouseMove>();
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, Mouse.transform.position, mouseSpeed);
        Vector3 mouselocation = Input.mousePosition;
        mouselocation.z = 5;
        transform.position = Camera.main.ScreenToWorldPoint(mouselocation);

	}
}
