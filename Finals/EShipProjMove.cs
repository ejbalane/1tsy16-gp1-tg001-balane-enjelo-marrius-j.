﻿using UnityEngine;
using System.Collections;

public class EShipProjMove : MonoBehaviour {
    public float velocity;
    private Vector3 direction;
    private GameObject myShip;
	// Use this for initialization
	void Start () {
        myShip = GameObject.FindGameObjectWithTag("myShip");
        direction = (myShip.transform.position - transform.position);
	}	
	// Update is called once per frame
    void Update() {
        GetComponent<Rigidbody>().velocity = direction * velocity * Time.deltaTime;
    }
}
