﻿using UnityEngine;
using System.Collections;

public class SharkFighter : MonoBehaviour{
    Transform MyShip;
    private float moveSpeed = 3;
    private float rotationSpeed = 3;
    Transform myTransform;
    public int HP;

    void Awake(){
        myTransform = transform;
    }

    void Start(){
        MyShip = GameObject.FindGameObjectWithTag("myShip").transform;
        HP = 5;
    }

    void Update(){
        myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(MyShip.position - myTransform.position), rotationSpeed * Time.deltaTime);
        myTransform.position += myTransform.forward * 2 * moveSpeed * Time.deltaTime;
    }

    void OnTriggerEnter3D(Collider coll) {
        HP--;
        if (HP==0)
            Destroy(gameObject);
        
    }
}
