﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    Vector3 PlayerPosition;
    Vector3 PlayerNewPosition;
    public float PlayerShipSpeed;
    public GameObject Cursor;
    public GameObject PlayerShip;


    // Use this for initialization
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {


        PlayerPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        transform.position = Vector3.Lerp(PlayerPosition, Cursor.transform.position, PlayerShipSpeed * Time.deltaTime);

    }
}

