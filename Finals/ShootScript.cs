﻿using UnityEngine;
using System.Collections;

public class ShootScript : MonoBehaviour {
    public GameObject Projectile;
    public Rigidbody projectile;
    public float speed = 20;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	//keyboard input
        if (Input.GetButtonDown("Fire1"))
        {
            Rigidbody instantiateProjectile = Instantiate(projectile, transform.position, transform.rotation) as Rigidbody;

            instantiateProjectile.velocity = transform.TransformDirection(new Vector3(0, speed, 0));
        }
	}
}
