﻿using UnityEngine;
using System.Collections;

public class EShipHP : MonoBehaviour {

    public float eHP;


	// Use this for initialization
	void Start () {
        eHP = 10;
	}
	
	
    void OnTriggerEnter(Collider coll) {
        if (coll.tag == "Bullet") {
            eHP--;
            Destroy(coll.gameObject);
            if (eHP == 0)
                Destroy(gameObject);
        }
    }
}
