﻿using UnityEngine;
using System.Collections;

public class ShipHealth : MonoBehaviour {
    private GameObject MyShip;
    public int startingHP = 10;
    public int currentHP;
    public float flashSpeed = 5f;
    public Color flashColor = new Color(1f, 0f, 0f, .1f);

    bool isDead;
    bool Damaged;

    void Awake() {
        currentHP = startingHP;
    }
	// Use this for initialization
	void Start () {
        MyShip = GameObject.FindGameObjectWithTag("myShip");
	
	}
	
	// Update is called once per frame
    public void TakeDamage(int amount) {
        Damaged = true;
        currentHP -= amount;

        if (currentHP <= 0 && !isDead)
        {
            Destroy(MyShip);
        }
    }
}
