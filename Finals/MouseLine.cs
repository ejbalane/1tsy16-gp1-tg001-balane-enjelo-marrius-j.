﻿using UnityEngine;
using System.Collections;

public class MouseLine : MonoBehaviour {
    public GameObject mouse;
	// Use this for initialization
	void Start () {	
	}	
	// Update is called once per frame
	void Update () {
        GameObject Player = GameObject.FindGameObjectWithTag ("myShip");
        GetComponent<LineRenderer> ().SetPosition(0, mouse.transform.position);
        GetComponent<LineRenderer> ().SetPosition(1, Player.transform.position);
	}
}
