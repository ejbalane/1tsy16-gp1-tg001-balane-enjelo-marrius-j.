﻿using UnityEngine;
using System.Collections;

public class EShipShoot : MonoBehaviour {
    private GameObject Eship;
    public float ProjVelocity;
    public int Proj;
    public GameObject EProj;
    public GameObject EProj1;
    public GameObject Eproj2;
	// Use this for initialization
	void Start () {
        StartCoroutine ("Projectile");
	}
    IEnumerator EnemyProjectile() {
        for (int i = 0; i < 1; i = 0) {
            switch (Proj) {
                case 1: yield return new WaitForSeconds(9f);
                    Instantiate(EProj, transform.position, Quaternion.identity);
                    break;
                case 2: yield return new WaitForSeconds(5f);
                    Instantiate(EProj1, transform.position, Quaternion.identity);
                    break;
                case 3: yield return new WaitForSeconds(4f);
                    Instantiate(Eproj2, transform.position, Quaternion.identity);
                    break;
                default: break;
            }
        }
    }
	
}
