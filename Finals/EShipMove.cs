﻿using UnityEngine;
using System.Collections;

public class EShipMove : MonoBehaviour {
    Transform MyShip;
    private float moveSpeed = 3;
    private float rotationSpeed = 3;
    Transform myTransform;

    void Awake()
    {
        myTransform = transform;
    }

    void Start()
    {
        MyShip = GameObject.FindGameObjectWithTag("myShip").transform;
    }

    void Update()
    {
        myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(MyShip.position - myTransform.position), rotationSpeed * Time.deltaTime);
        myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
    }
}
