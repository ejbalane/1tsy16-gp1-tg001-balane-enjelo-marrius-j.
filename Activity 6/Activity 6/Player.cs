﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity_6
{
    class Player
    {
        public string Name = "Hero";
        public int HP = 100;
        public int attack = 1;
        public bool dead = false;

        public void GiveDamage(Monster m)
        {
            m.TakeDamage(attack);
        }

        public void TakeDamage(int Damage)
        {
            HP -= Damage;
        }
        public void Died()
        {
            if (HP <= 0)
            {
                dead = true;
                for (int i = 0; i <= 5; i++)
                {
                    Console.WriteLine("You Died");
                }                
            }
        }
    }

}
        
