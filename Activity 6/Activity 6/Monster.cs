﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity_6
{
    class Monster
    {
        public string Name = "Monster";
        public int HP = 10;
        public int attack = 1;
        public bool Dead = false;

        public void GiveDamage(Player p)
        {
            p.TakeDamage(attack);
        }

        public void TakeDamage(int Damage)
        {
            HP -= Damage;
        }

        public void Died()
        {
            if (HP<=0)
            {
                Dead = true;
                Console.WriteLine("The Monster has Died");
            }
        }
        
    }
}
