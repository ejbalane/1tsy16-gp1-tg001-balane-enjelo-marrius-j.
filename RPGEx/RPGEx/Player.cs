﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    
    public class Player
    {
        public string Name { get; private set; }
        public string Race { get; private set; }
        public int MagicPoints;
        // Constructor
        public Player()
        {
            Race = "Default";
            Name = "Default";
            className = "Default";
            accuracy = 0;
            hitPoints = 0;
            maxHitPoints = 0;
            expPoints = 0;
            nextLevelExp = 0;
            level = 0;
            Armor = 0;
            weapon = new Weapon("Default Weapon Name", 0, 0);
            MagicPoints = 0;
        }

       

        public void CreateClass()
        {
            Console.WriteLine("CHARACTER CLASS GENERATION");
            Console.WriteLine("==========================");

            // Input character's name.
            Console.WriteLine("Enter your character's name: ");
            Name = Console.ReadLine();

            //Enter Race
            Console.WriteLine("Plese choose a race you want to be:");
            Console.WriteLine("1.) Human 2.) Elf 3.) Dwarf 4.)Orc");

            int Racenum = 1;

            Racenum = Convert.ToInt32(Console.ReadLine());

            switch (Racenum)
            {
                case 1: //Human
                    accuracy = +10;
                    Armor = +6;
                    maxHitPoints = 20;
                    break;

                case 2: //Elf
                    accuracy = +20;
                    Armor = +5;
                    maxHitPoints = 12;
                    break;

                case 3: //Dwarf
                    accuracy = +5;
                    Armor = +7;
                    maxHitPoints = 21;
                    break;

                default: //Orc
                    accuracy = +7;
                    Armor = +3;
                    maxHitPoints = 30;
                    break;

            }


            Console.WriteLine("Please select a character class number...");
            Console.WriteLine("1)Fighter 2)Wizard 3)Cleric 4)Thief : ");

            int characterNum = 1;

            characterNum = Convert.ToInt32(Console.ReadLine());

            switch (characterNum)
            {
                case 1: //Fighter
                    className = "Fighter";
                    hitPoints = 20;
                    accuracy = 10;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    Armor = 20;
                    level = 1;
                    weapon = new Weapon("Long Sword", 1, 8);
                    MagicPoints = 10;
                    break;
                case 2: //Wizard
                    className = "Wizard";
                    hitPoints = 10;
                    expPoints = 0;
                    Armor = 10;
                    nextLevelExp = 1000;
                    level = 1;
                    MagicPoints = 20;
                    weapon = new Weapon("Staff", 1, 4);
                    break;
                case 3: //Cleric
                    className = "Cleric";
                    hitPoints = 15;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 15;
                    weapon = new Weapon("Flail", 1, 6);
                    MagicPoints = 15;
                    break;
                default: //Thief
                    className = "Thief";
                    hitPoints = 12;
                    expPoints = 0;
                    nextLevelExp = 1000;
                    level = 1;
                    Armor = 17;
                    weapon = new Weapon("Dagger", 1, 6);
                    MagicPoints = 12;
                    break;
            }

        }

        public bool isDead { get { return hitPoints <= 0; } }
        public int Armor { get; private set; }
        public bool Attack(Monster monsterTarget)
        {
            int selection = 1;
            Console.Write("1) Attack, 2) Cast Spell, 3) Run");
            selection = Convert.ToInt32(Console.ReadLine());
            switch (selection)
            {
                case 1:
                    Console.WriteLine("You attack an " + monsterTarget.Name + " with a " + weapon.Name);
                    if (RandomHelper.Random(0, 20) < accuracy)
                    {
                        int damage = RandomHelper.Random(weapon.DamageRange);
                        int totalDamage = damage - monsterTarget.Armor;
                        if (totalDamage <= 0)
                        {
                            Console.WriteLine("Your attack failed to penetrate the armor");
                        } else
                        {
                            Console.WriteLine("You attack for " + totalDamage.ToString() + " damage!");
                            monsterTarget.TakeDamage(totalDamage);
                        }
                    }
                    else
                    {
                        Console.WriteLine("You miss!");
                    }
                    break;
                case 2:
                    if (MagicPoints > 0)
                    {
                      
                        Console.WriteLine("What spell would you like to cast?");
                        Console.WriteLine("1) Fireball 2) Nether Strike 3) Nether Bomb 4) Heal");

                        int spellnum = Convert.ToInt32(Console.ReadLine());
                        Spell spell = null;
                        int damage = RandomHelper.Random(spell.DamageRange);
                        int totalDamage = damage;
                        
                        switch(spellnum)
                        {
                            case 1:
                                Console.WriteLine("You used Fireball");
                                spell = new Spell("Fireball" , 5, 10,1);
                                monsterTarget.TakeDamage(damage);
                                MagicPoints--;
                                break;
                            case 2:
                                Console.WriteLine("You used Nether Strike");
                                spell = new Spell("Nether Strike", 11, 15, 3);
                                monsterTarget.TakeDamage(damage);
                                for (int A = 0; A <= 2; A++)
                                {
                                    MagicPoints--;
                                }
                                break;
                            default:
                                Console.WriteLine("You used Nether Bomb");
                                spell = new Spell("Nether Bomb", 16, 20, 5);
                                monsterTarget.TakeDamage(damage);
                                for (int B = 0; B <= 4; B++)
                                {
                                    MagicPoints--;
                                }
                                break;
                            case 4:
                                Console.WriteLine("You use heal");
                                spell = new Spell("Heal", 21, 25, 10);
                                hitPoints = +10;
                                for (int C = 0; C <=9 ; C++)
                                {
                                    MagicPoints--;
                                }
                                break;
                            case 5:
                                Console.WriteLine("ALLAHU AKBAR!!!!");
                                spell = new Spell("Allahu Akbar", 99, 100, 10);// brings your health to 1
                                // and kills your enemy automaticallyjv
                                hitPoints = 1;
                                monsterTarget.TakeDamage(damage);
                                for (int D = 0; D <= 9 ; D++)
                                {
                                    MagicPoints--;
                                }
                                break;
                        }
                        Console.WriteLine("You have dealt" + totalDamage.ToString() + "damage");
                        
                    }

                    else
                    {
                        Console.WriteLine("Not enough Mana");
                    }
                    break;
                case 3:
                    // 25% chance of being able to run
                    int roll = RandomHelper.Random(1, 4);
                    if (roll == 1)
                    {
                        Console.WriteLine("You run away!");
                        return true; //<-- Return out of the function
                    }
                    else
                    {
                        Console.WriteLine("You could not escape!");
                    }
                    break;
            }

            return false;
        }

        public void TakeDamage(int damage)
        {
            hitPoints -= damage;
            if (hitPoints < 0)
            {
                hitPoints = 0;
            }
        }

        public void LevelUp()
        {
            if (expPoints >= nextLevelExp)
            {
                Console.WriteLine("You gained a level!");

                // Increment level.
                level++;

                // Set experience points requard for next level
                nextLevelExp = level * level * 1000;

                // Increase stats randomly
                if(className == "Fighter")
                {
                    Armor += RandomHelper.Random(1, 2);
                }
                else if (className == "Wizard")
                {
                    maxHitPoints += RandomHelper.Random(1, 5);
                }

                else if (className == "Cleric")
                {
                    accuracy += RandomHelper.Random(1, 3);
                }
                else
                {
                    Armor += RandomHelper.Random(3, 5);
                }

                accuracy += RandomHelper.Random(1, 3);
                maxHitPoints += RandomHelper.Random(2, 6);
                

                // Give the player full hitpoints when they level up.
                hitPoints = maxHitPoints;
            }
        }

        public void Rest()
        {
            

            int roll = RandomHelper.Random(1, 4);
           /* if (roll == 1)
           {
                Console.WriteLine("You cannot rest");
                return 1;
            }
            else
            {
                Console.WriteLine("Resting...");

                hitPoints = maxHitPoints;
            }
            */
            // TODO: Modify the function so that random enemy enounters
            // are possible when resting

        }

        

        public void ViewStats()
        {
            Console.WriteLine("PLAYER STATS");
            Console.WriteLine("============");
            Console.WriteLine();

            Console.WriteLine("Name             = " + Name);
            Console.WriteLine("Class            = " + className);
            Console.WriteLine("Accuracy         = " + accuracy.ToString());
            Console.WriteLine("Hitpoints        = " + hitPoints.ToString());
            Console.WriteLine("MaxHitpoints     = " + maxHitPoints.ToString());
            Console.WriteLine("XP               = " + expPoints.ToString());
            Console.WriteLine("XP to next level = " + nextLevelExp.ToString());
            Console.WriteLine("Level            = " + level.ToString());
            Console.WriteLine("Armor            = " + Armor.ToString());
            Console.WriteLine("Weapon Name      = " + weapon.Name);
            Console.WriteLine("Weapon Damage    = " + weapon.DamageRange.Low.ToString() + "-" + weapon.DamageRange.High.ToString());
            Console.WriteLine("Gold             = " + Goldplayer);
            Console.WriteLine("Mana             = " + MagicPoints);

            Console.WriteLine();
            Console.WriteLine("END PLAYER STATS");
            Console.WriteLine("================");
            Console.WriteLine();
        }
        
        public void Victory(int xp)
        {
            Console.WriteLine("You won the battle!");
            Console.WriteLine("You win " + xp.ToString() + " experience points!");
            expPoints += xp;
            Goldplayer += Goldplayer;
            Console.WriteLine("You recieved" + Goldplayer/2 + "gold");
        }

        public void GameOver()
        {
            Console.WriteLine("You died in battle...");
            Console.WriteLine();
            Console.WriteLine("==========================");
            Console.WriteLine("GAME OVER!");
            Console.WriteLine("==========================");
            Console.ReadLine();
        }

        public void DisplayHitPoints()
        {
            Console.WriteLine(Name + "'s hitpoints = " + hitPoints.ToString());
        }
        
        private string className;
        private int hitPoints;
        private int maxHitPoints;
        private int expPoints;
        private int nextLevelExp;
        private int level;
        private int accuracy;
        private Weapon weapon;
        private int Goldplayer = 10;
        private int Mana;
    }
}
