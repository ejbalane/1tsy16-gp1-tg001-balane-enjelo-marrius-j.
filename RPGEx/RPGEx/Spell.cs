﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Spell
    {
        public Spell( string name, int lowDamage, int highDamage, int MPCost)
        {
            Name = name;
            DamageRange.Low = lowDamage;
            DamageRange.High = highDamage;
            ManaRequired = MPCost;
        }

        //constructor
        public string Name { get; set; }
        public Range DamageRange;
        public int ManaRequired;   
    }
}
